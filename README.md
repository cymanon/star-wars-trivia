# About the project
This project is a simple take on translating **[SWAPI](https://swapi.dev/)** into visible content as programmatically as possible.
Project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Big thanks to:

[SWAPI](https://swapi.dev/) and 
[Jeremy Perkins](https://unsplash.com/@jeremyperkins?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/stars?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

