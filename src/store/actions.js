import * as cons from '../constants'
import createMultipleFetch from '../utils'

export const addFilms = () => {
    return dispatch => fetch(`${cons.API_ADDRESS}films/`)
        .then(response => response.json())
        .then(data => {
            dispatch({ type: cons.ADD_FILMS, payload: [...data.results] });
        })
}

export const addPeople = () => {
    return dispatch => createMultipleFetch('people')
        .then(data => { 
            const dataArray = []
            data.forEach(d => dataArray.push(...d))
            dispatch({ type: cons.ADD_PEOPLE, payload: dataArray });
        })
}

export const addPlanets = () => {
    return dispatch => createMultipleFetch('planets')
        .then(data => { 
            const dataArray = []
            data.forEach(d => dataArray.push(...d))
            dispatch({ type: cons.ADD_PLANETS, payload: dataArray });
        })
}

export const addStarships = () => {
    return dispatch => createMultipleFetch('starships')
        .then(data => { 
            const dataArray = []
            data.forEach(d => dataArray.push(...d))
            dispatch({ type: cons.ADD_STARSHIP, payload: dataArray });
        })
}

export const addVehicles = () => {
    return dispatch => createMultipleFetch('vehicles')
        .then(data => { 
            const dataArray = []
            data.forEach(d => dataArray.push(...d))
            dispatch({ type: cons.ADD_VEHICLES, payload: dataArray });
        })
}

export const addSpecies = () => {
    return dispatch => createMultipleFetch('species')
        .then(data => { 
            const dataArray = []
            data.forEach(d => dataArray.push(...d))
            dispatch({ type: cons.ADD_SPECIES, payload: dataArray });
        })
}
