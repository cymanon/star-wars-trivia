import * as actions from '../constants'

const initialState = {
    films: [],
    people: [],
    planets: [],
    starships: [],
    vehicles: [],
    species: []
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.ADD_FILMS:
            return {
                ...state,
                films: [
                    ...state.films,
                    ...action.payload
                ]
            }

        case actions.ADD_PEOPLE:
            return {
                ...state,
                people: [
                    ...state.people,
                    ...action.payload
                ]
            }
        case actions.ADD_PLANETS:
            return {
                ...state,
                planets: [
                    ...state.planets,
                    ...action.payload
                ]
            }
        case actions.ADD_STARSHIP:
            return {
                ...state,
                starships: [
                    ...state.starships,
                    ...action.payload
                ]
            }
        case actions.ADD_VEHICLES:
            return {
                ...state,
                vehicles: [
                    ...state.vehicles,
                    ...action.payload
                ]
            }
        case actions.ADD_SPECIES:
            return {
                ...state,
                species: [
                    ...state.species,
                    ...action.payload
                ]
            }

        default:
            break
    }
    return state
}

export default rootReducer