import React from 'react'

const Search = () => {
    return (
        <div className='component no-transform '>
            <h1>About the project</h1>
            <p>This is a simple take on translating the API into visible content content as programmatically as possible.</p>
            <h3>Thanks to:</h3>
            <p><a href="https://swapi.dev/">SWAPI</a></p>
            <p><a href="https://unsplash.com/@jeremyperkins?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Jeremy Perkins</a> on <a href="https://unsplash.com/s/photos/stars?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a></p>
        </div>
    )
}

export default Search
