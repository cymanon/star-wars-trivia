import React from 'react'
import { useSelector } from 'react-redux'
import { namesFinder } from './utils'

const CreateComponent = ({ name }) => {
    const films = useSelector(state => state.films)
    const people = useSelector(state => state.people)
    const vehicles = useSelector(state => state.vehicles)
    const planets = useSelector(state => state.planets)
    const species = useSelector(state => state.species)
    const starships = useSelector(state => state.starships)

    let componentDisplay = <p>Loading</p>

    let arrayToDisplay
    switch (name) {
        case 'films':
            arrayToDisplay = films
            break
        case 'people':
            arrayToDisplay = people
            break
        case 'vehicles':
            arrayToDisplay = vehicles
            break
        case 'planets':
            arrayToDisplay = planets
            break
        case 'species':
            arrayToDisplay = species
            break
        case 'starships':
            arrayToDisplay = starships
            break
        default:
            break
    }

   if (arrayToDisplay) {
       componentDisplay = arrayToDisplay.map(singleItem => {
           const forbiddenWords = ['edited', 'created', 'url']
           const keys = Object.keys(singleItem).map(key =>
               !forbiddenWords.some(word => word === key) && key)

           const display = keys.map((key, index) => {
               const regex = /_/g
               let keyCleaned = key
               if (!key) {
                   return null
               }
               if (typeof key === 'string') {
                   keyCleaned = key.replace(regex, ' ')
               }
               let value = singleItem[key]

               if (key === 'films') {
                   const filmsDisplay = namesFinder(singleItem[key], films, 'title')
                   value = filmsDisplay
               }

               if (key === 'pilots' || key === 'characters' || key === 'residents' || key === 'people' ) {
                   const pilotsDisplay = namesFinder(singleItem[key], people)
                   value = pilotsDisplay
               }

               if (key === 'species') {
                   const speciesDisplay = namesFinder(singleItem[key], species)
                   value = speciesDisplay
               }
               if (key === 'starships') {
                   const starshipsDisplay = namesFinder(singleItem[key], starships)
                   value = starshipsDisplay
               }
               if (key === 'vehicles') {
                   const vehiclesDisplay = namesFinder(singleItem[key], vehicles)
                   value = vehiclesDisplay
               }
               if (key === 'planets' || key === 'homeworld' ) {
                   const planetsDisplay = namesFinder(singleItem[key], planets)
                   value = planetsDisplay
               }

               return (
                   <p key={index}>{keyCleaned}: {value}</p>
               )
           })

           return display
       })

       return (
           <div className='component'>
               {componentDisplay}
           </div>
       )
   }
}

export default CreateComponent