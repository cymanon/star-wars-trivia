import CreateComponent from './createComponent'

export const Films = () => <CreateComponent name='films' />
export const People = () => <CreateComponent name='people' />
export const Planets = () => <CreateComponent name='planets' />
export const Species = () => <CreateComponent name='species' />
export const Starships = () => <CreateComponent name='starships' />
export const Vehicles = () => <CreateComponent name='vehicles' />