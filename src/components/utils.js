export const namesFinder = (urlsArray, objectsArray, property = 'name') => {
    const urls = Array.isArray(urlsArray) ? [...urlsArray] : [urlsArray]
    let namesArray
    if (!urls.length) {
        namesArray = 'none'
    } else {
        namesArray = urls.map(url => {
            const currentObject = objectsArray.find(obj => obj.url === url)

            let value = currentObject && currentObject[property]
            if (!urls.length) {
                value = 'none'
            }

            return value
        })
    }

    return typeof namesArray === 'object' ? namesArray.join(', ') : namesArray
}


