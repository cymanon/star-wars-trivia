export const API_ADDRESS = 'https://swapi.dev/api/' 

export const ADD_FILMS = 'ADD_FILMS'
export const ADD_PEOPLE = 'ADD_PEOPLE'
export const ADD_PLANETS = 'ADD_PLANETS'
export const ADD_SPECIES = 'ADD_SPECIES'
export const ADD_STARSHIP = 'ADD_STARSHIP'
export const ADD_VEHICLES = 'ADD_VEHICLES'
