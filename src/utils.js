import { API_ADDRESS } from './constants'

async function createMultipleFetch(category) {
    let isThereNextOne = true
    let count = 1
    const allData = []
    do {
        // eslint-disable-next-line no-loop-func
        const response = await fetch(`${API_ADDRESS}${category}/?page=${count}`)
        const finalResponse = await response.json()
        isThereNextOne = finalResponse.next ? true : false
        count++
        allData.push(finalResponse.results)

    } while (isThereNextOne)


    return allData
}

export default createMultipleFetch