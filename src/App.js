import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as dispatches from './store/actions'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

import './App.css'

import { Films, People, Planets, Species, Starships, Vehicles } from './components/components'
import About from './components/about'

class App extends Component {

  componentDidMount() {
    this.props.addFilms()
    this.props.addPeople()
    this.props.addPlanets()
    this.props.addStarships()
    this.props.addVehicles()
    this.props.addSpecies()
  }

  render() {
    return (
      <div>
        <Router>
          <div className='navigation-bar'>
            <Link to='/'>About</Link>
            <Link to='/films'>Films</Link>
            <Link to='/people'>People</Link>
            <Link to='/planets'>Planets</Link>
            <Link to='/species'>Species</Link>
            <Link to='/starships'>Starships</Link>
            <Link to='/vehicles'>Vehicles</Link>
          </div>

          <Switch>
            <Route path="/films"><Films /></Route>
            <Route path="/people"><People /></Route>
            <Route path="/planets"><Planets /></Route>
            <Route path="/species"><Species /></Route>
            <Route path="/starships"><Starships /></Route>
            <Route path="/vehicles"><Vehicles /></Route>
            <Route path="/"><About /></Route>
          </Switch>
        </Router >
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addFilms: () => dispatch(dispatches.addFilms()),
    addPeople: () => dispatch(dispatches.addPeople()),
    addPlanets: () => dispatch(dispatches.addPlanets()),
    addStarships: () => dispatch(dispatches.addStarships()),
    addVehicles: () => dispatch(dispatches.addVehicles()),
    addSpecies: () => dispatch(dispatches.addSpecies()),
  }
}

export default connect(null, mapDispatchToProps)(App);
